<?xml version="1.0" encoding="UTF-8"?>
<Graph author="branislav.repcek@besouldata.com" category="readers" created="Tue Dec 08 17:33:42 CET 2015" guiVersion="4.1.0" id="1449661596477" largeIconPath="${SUBGRAPH_DIR}/Transactions_64.png" licenseCode="CLP1DJAVLI13741463BY" licenseType="Commercial" mediumIconPath="${SUBGRAPH_DIR}/Transactions_32.png" modified="Wed Jan 06 13:46:33 EST 2016" modifiedBy="angie" name="TransactionReader" nature="subgraph" revision="1.35" showComponentDetails="true" smallIconPath="${SUBGRAPH_DIR}/Transactions_16.png">
<Global>
<inputPorts>
<singlePort connected="true" keepEdge="false" name="0" required="false"/>
</inputPorts>
<outputPorts>
<singlePort connected="true" name="0"/>
<singlePort connected="true" keepEdge="true" name="1" required="false"/>
</outputPorts>
<Metadata fileURL="${META_DIR}/Transaction.fmt" id="Metadata0"/>
<Metadata id="Metadata3">
<Record fieldDelimiter="|" name="FileURL" recordDelimiter="\r\n" type="delimited">
<Field name="fileURL" type="string"/>
</Record>
</Metadata>
<Metadata id="Metadata2" previewAttachmentCharset="ISO-8859-1">
<Record fieldDelimiter="," name="TransactionInputError" previewAttachmentCharset="ISO-8859-1" recordDelimiter="\r\n" type="delimited">
<Field name="recordNo" type="long"/>
<Field name="inputFileURL" type="string"/>
<Field name="fieldName" type="string"/>
<Field name="errorMessage" type="string"/>
<Field name="transactionId" type="long"/>
</Record>
</Metadata>
<Metadata id="Metadata1" previewAttachmentCharset="ISO-8859-1">
<Record fieldDelimiter="," name="TransactionInputExt" previewAttachmentCharset="ISO-8859-1" recordDelimiter="\r\n" skipSourceRows="1" type="delimited">
<Field name="transactionId" type="long"/>
<Field name="accountNumber" type="string"/>
<Field name="transactionType" type="string"/>
<Field length="20" name="amount" scale="3" type="decimal"/>
<Field name="timestamp" type="string"/>
<Field auto_filling="source_row_count" name="recordNo" type="long"/>
<Field auto_filling="source_name" name="fileName" type="string"/>
</Record>
</Metadata>
<MetadataGroup name="Implicit metadata" type="implicit">
<Metadata id="__static_metadata_DATA_READER_FlatFileReader_Error">
<Record fieldDelimiter="|" name="FlatFileReader_Error" recordDelimiter="\n" type="delimited">
<Field name="recordNo" trim="true" type="long"/>
<Field name="fieldNo" trim="true" type="integer"/>
<Field name="originalData" type="string"/>
<Field name="errorMessage" type="string"/>
<Field name="fileURL" type="string"/>
</Record>
</Metadata>
</MetadataGroup>
<GraphParameters>
<GraphParameter label="Transactions File URL" name="INPUT_FILE_URL" public="true" required="true" value="${DATAIN_DIR}/transactions-20150101.csv">
<attr name="description"><![CDATA[URL of the file to process]]></attr>
<ComponentReference referencedComponent="TRANSACTIONS" referencedProperty="fileURL"/>
</GraphParameter>
<GraphParameter label="Input encoding" name="ENCODING" public="true" required="false">
<attr name="description"><![CDATA[Encoding of the input file.]]></attr>
<ComponentReference referencedComponent="TRANSACTIONS" referencedProperty="charset"/>
</GraphParameter>
<GraphParameter category="basic" label="Data policy" name="DATA_POLICY" public="true" value="Controlled">
<attr name="description"><![CDATA[Configures what would happen if errors are found.
Lenient: errors are ignored and logged
Controlled (default): errors are sent to error port
Strict: subgraph will fail on first error]]></attr>
<SingleType allowCustomValues="false" name="simpleEnum" values="Lenient;Controlled;Strict"/>
</GraphParameter>
<GraphParameterFile fileURL="workspace.prm"/>
</GraphParameters>
<Note alignment="1" backgroundColorB="225" backgroundColorG="255" backgroundColorR="255" folded="false" height="127" id="Note0" textColorB="0" textColorG="0" textColorR="0" textFontSize="8" title="Making subgraph nice" titleColorB="0" titleColorG="0" titleColorR="0" titleFontSize="10" width="576" x="111" y="72">
<attr name="text"><![CDATA[* Assign category to the subgraph in Properties view (click on an empty place and go to Properties view at the bottom of the screen).
* Assign icons to the subgraph.]]></attr>
</Note>
<Note alignment="1" backgroundColorB="225" backgroundColorG="255" backgroundColorR="255" folded="false" height="171" id="Note1" textColorB="0" textColorG="0" textColorR="0" textFontSize="8" title="Optional output port" titleColorB="0" titleColorG="0" titleColorR="0" titleFontSize="10" width="222" x="1024" y="392">
<attr name="text"><![CDATA[Note the second port - it is set to be optional and will discard all records if it is not connected.
This means that parent graph can choose to ignore the error records simply by not connecting the port.]]></attr>
</Note>
<Note alignment="1" backgroundColorB="225" backgroundColorG="255" backgroundColorR="255" folded="false" height="123" id="Note2" textColorB="0" textColorG="0" textColorR="0" textFontSize="8" title="Data policy" titleColorB="0" titleColorG="0" titleColorR="0" titleFontSize="10" width="333" x="632" y="408">
<attr name="text"><![CDATA[Data policy is implemented in simple way - the reader still uses controlled policy and we decide what to do with the record afterwards in Reformat component.]]></attr>
</Note>
<Note alignment="1" backgroundColorB="225" backgroundColorG="255" backgroundColorR="255" folded="false" height="256" id="Note3" textColorB="0" textColorG="0" textColorR="0" textFontSize="8" title="Optional input port" titleColorB="0" titleColorG="0" titleColorR="0" titleFontSize="10" width="263" x="-253" y="295">
<attr name="text"><![CDATA[Note that the port is set to "remove edge if not connected".
This configuration is in line with usual behaviour for readers where it is possible to either read file based on property value or pass the data via port (in-memory).
If this port is not connected, the port going to FlatFileReader will be removed and the subgraph will require file URL to be specified in INPUT_FILE_URL parameter.]]></attr>
</Note>
<Dictionary/>
</Global>
<Phase number="0">
<Node guiName="Apply data policy" guiX="761" guiY="304" id="APPLY_DATA_POLICY" type="REFORMAT">
<attr name="transform"><![CDATA[//#CTL2

function integer transform() {

	if ("${DATA_POLICY}" == "Controlled") {
		$out.0.* = $in.0.*;
		return OK;
	} else if ("${DATA_POLICY}" == "Lenient") {
		// Print the error message and ignore the record
		printLog(error, errorToString());
		return SKIP;
	} else { // Strict
		raiseError(errorToString());
	}
}

function string errorToString() {
	return "Record " + $in.0.recordNo + ", field \"" + $in.0.fieldName + "\": " + $in.0.errorMessage;
}]]></attr>
</Node>
<Node debugOutput="true" guiName="Errors" guiX="1066" guiY="304" id="ERRORS" type="TRASH"/>
<Node enabled="enabled" guiName="Fix errors" guiX="343" guiY="211" id="FIX_ERRORS" type="REFORMAT">
<attr name="transform"><![CDATA[//#CTL2

// Name of the field currently processed. It is not possible to pass this in the error message,
// so we keep this as global variable.
// This value is used transformOnError in case the transformation failed.
string currentFieldName;

function integer transform() {

	currentFieldName = null; // Reset field name
	$out.0.* = $in.0.*;
	
	currentFieldName = "accountNumber";
	// Remove dash from account number
	string accountNum = replace($in.0.accountNumber, "-", "");
	$out.0.accountNumber = str2long(accountNum);
	
	currentFieldName = "timestamp";
	// Parse timestamp in two different formats. Any other value is considered and error.
	switch (length($in.0.timestamp)) {
		case 19:
			$out.0.timestamp = str2date($in.0.timestamp, "yyyy-MM-dd hh:mm:ss");
			break;
			
		case 14:
			$out.0.timestamp = str2date($in.0.timestamp, "yyyyMMddhhmmss");
			break;

		default:
			raiseError("Unsupported input date format for transaction " + $in.0.transactionId + ", value=\"" + $in.0.timestamp + "\".");
	}

	return OK;
}

function integer transformOnError(string errorMessage, string stackTrace) {
	// First remove extra text from the error message
	integer ctlFragmentStart = indexOf(errorMessage, "\n------");
	
	if (ctlFragmentStart > 0) {
		errorMessage = trim(left(errorMessage, ctlFragmentStart)); // Also get the newline.
	}
	
	$out.1.inputFileURL = $in.0.fileName;
	$out.1.recordNo = $in.0.recordNo;
	$out.1.fieldName = currentFieldName;
	$out.1.errorMessage = errorMessage;
	$out.1.transactionId = $in.0.transactionId;
	
	return 1;
}
]]></attr>
<attr name="guiDescription"><![CDATA[Attempt to fix parsing errors in the data.]]></attr>
</Node>
<Node guiName="Fix parsing errors" guiX="343" guiY="431" id="FIX_PARSING_ERRORS" type="REFORMAT">
<attr name="transform"><![CDATA[//#CTL2
// This record is used to figure out field name.
TransactionInputExt temp;

function integer transform() {
	$out.0.inputFileURL = $in.0.fileURL;
	$out.0.errorMessage = $in.0.errorMessage;
	$out.0.recordNo = $in.0.recordNo;
	$out.0.fieldName = getFieldName(temp, $in.0.fieldNo);

	return ALL;
}
]]></attr>
</Node>
<Node debugInput="true" guiName="Generate file URL" guiX="-192" guiY="211" id="GENERATE_FILE_URL" type="DATA_GENERATOR">
<attr name="generate"><![CDATA[//#CTL2

function integer generate() {
	// Hardcoded name useful for testing.
	$out.0.fileURL = "${DATAIN_DIR}/transactions-20150101.csv";
	return ALL;
}
]]></attr>
</Node>
<Node guiName="SimpleGather" guiX="579" guiY="304" id="SIMPLE_GATHER" type="SIMPLE_GATHER"/>
<Node guiName="SubgraphInput" guiX="24" guiY="7" id="SUBGRAPH_INPUT" type="SUBGRAPH_INPUT">
<Port guiY="230" name="0"/>
<Port guiY="300" name="1"/>
</Node>
<Node guiName="SubgraphOutput" guiX="987" guiY="7" id="SUBGRAPH_OUTPUT" type="SUBGRAPH_OUTPUT">
<Port guiY="228" name="0"/>
<Port guiY="322" name="1"/>
<Port guiY="446" name="2"/>
</Node>
<Node charset="${ENCODING}" dataPolicy="controlled" enabled="enabled" fileURL="${INPUT_FILE_URL}" guiName="Transactions" guiX="111" guiY="211" id="TRANSACTIONS" type="FLAT_FILE_READER"/>
<Node debugOutput="true" guiName="Valid records" guiX="1066" guiY="211" id="VALID_RECORDS" type="TRASH"/>
<Edge fromNode="APPLY_DATA_POLICY:0" guiBendpoints="" guiRouter="Manhattan" id="Edge1" inPort="Port 1 (in)" outPort="Port 0 (out)" persistedImplicitMetadata="Metadata2" toNode="SUBGRAPH_OUTPUT:1"/>
<Edge debugMode="true" fromNode="FIX_ERRORS:0" guiBendpoints="" guiRouter="Manhattan" id="Edge5" inPort="Port 0 (in)" metadata="Metadata0" outPort="Port 0 (out)" toNode="SUBGRAPH_OUTPUT:0"/>
<Edge debugMode="true" fromNode="FIX_ERRORS:1" guiBendpoints="" guiRouter="Manhattan" id="Edge0" inPort="Port 0 (in)" metadata="Metadata2" outPort="Port 1 (out)" toNode="SIMPLE_GATHER:0"/>
<Edge fromNode="FIX_PARSING_ERRORS:0" guiBendpoints="" guiRouter="Manhattan" id="Edge7" inPort="Port 1 (in)" metadata="Metadata2" outPort="Port 0 (out)" toNode="SIMPLE_GATHER:1"/>
<Edge fromNode="GENERATE_FILE_URL:0" guiBendpoints="" guiRouter="Manhattan" id="Edge11" inPort="Port 0 (in)" metadata="Metadata3" outPort="Port 0 (out)" toNode="SUBGRAPH_INPUT:0"/>
<Edge debugMode="true" fromNode="SIMPLE_GATHER:0" guiBendpoints="" guiRouter="Manhattan" id="Edge8" inPort="Port 0 (in)" outPort="Port 0 (out)" persistedImplicitMetadata="Metadata2" toNode="APPLY_DATA_POLICY:0"/>
<Edge fromNode="SUBGRAPH_INPUT:0" guiBendpoints="" guiRouter="Manhattan" id="Edge6" inPort="Port 0 (input)" outPort="Port 0 (out)" persistedImplicitMetadata="Metadata3" toNode="TRANSACTIONS:0"/>
<Edge fromNode="SUBGRAPH_OUTPUT:0" guiBendpoints="" guiRouter="Manhattan" id="Edge10" inPort="Port 0 (in)" outPort="Port 0 (out)" persistedImplicitMetadata="Metadata0" toNode="VALID_RECORDS:0"/>
<Edge fromNode="SUBGRAPH_OUTPUT:1" guiBendpoints="" guiRouter="Manhattan" id="Edge9" inPort="Port 0 (in)" outPort="Port 1 (out)" persistedImplicitMetadata="Metadata2" toNode="ERRORS:0"/>
<Edge debugMode="false" fromNode="TRANSACTIONS:0" guiBendpoints="352:306|352:304" guiLocks="453|null|null" guiRouter="Manhattan" id="Edge2" inPort="Port 0 (in)" metadata="Metadata1" outPort="Port 0 (output)" toNode="FIX_ERRORS:0"/>
<Edge debugMode="true" fromNode="TRANSACTIONS:1" guiBendpoints="" guiRouter="Manhattan" id="Edge4" inPort="Port 0 (in)" outPort="Port 1 (logs)" persistedImplicitMetadata="__static_metadata_DATA_READER_FlatFileReader_Error" toNode="FIX_PARSING_ERRORS:0"/>
</Phase>
</Graph>
