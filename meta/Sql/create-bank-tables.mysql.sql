create table customers (
  customer_id decimal(10, 0) not null unique,
  first_name varchar(100) not null,
  last_name varchar(100) not null,
  street varchar(100),
  city varchar(100),
  zip_code varchar(20),
  state varchar(40),
  created_date date not null,
  closed_date date
);

create table accounts (
  account_id decimal(19, 0) not null unique,
  customer_id decimal(10, 0) not null,
  balance decimal(20, 3) not null,
  created date,
  closed date,
  
  constraint accounts_fk_cust_id foreign key (customer_id) references customers(customer_id) on delete cascade
);

create table transactions (
  trans_id decimal(19, 0) not null unique,
  account_id decimal(19, 0) not null,
  trans_type char(1) not null,
  amount decimal(20, 3) not null,
  trans_time date not null,
  
  constraint trans_fk_acc_id foreign key (account_id) references accounts(account_id) on delete cascade
);
