create table customers (
  customer_id number(10, 0) not null unique,
  first_name varchar2(100) not null,
  last_name varchar2(100) not null,
  street varchar2(100),
  city varchar2(100),
  zip_code varchar2(20),
  state varchar2(40),
  created_date date not null,
  closed_date date
);

create table accounts (
  account_id number(19, 0) not null unique,
  customer_id number(10, 0) not null,
  balance number(20, 3) not null,
  created date,
  closed date,
  
  constraint accounts_fk_cust_id foreign key (customer_id) references customers(customer_id)
);

create table transactions (
  trans_id number(19, 0) not null unique,
  account_id number(19, 0) not null,
  trans_type char(1) not null,
  amount number(20, 3) not null,
  trans_time date not null,
  
  constraint trans_fk_acc_id foreign key (account_id) references accounts(account_id)
);
