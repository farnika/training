drop table if exists state_codes;

create table state_codes (
  state_code varchar(2) not null primary key,
  state_name varchar(100) not null
);