create table state_codes (
  state_code varchar2(2) not null primary key,
  state_name varchar2(100) not null
);