import org.jetel.component.DataRecordTransform;
import org.jetel.data.DataRecord;
import org.jetel.data.primitive.HugeDecimal;
import org.jetel.exception.TransformException;

public class CalculateFee extends DataRecordTransform {

	private static final HugeDecimal FEE_FRACTION;

	static {
		FEE_FRACTION = new HugeDecimal(null, 20, 3, false);
		FEE_FRACTION.setValue(0.005D);
	}
	
	@Override
	public int transform(DataRecord[] input, DataRecord[] output) throws TransformException {
		output[0].copyFieldsByName(input[0]);
		
		HugeDecimal amount =(HugeDecimal) input[0].getField("amount").getValue();
		amount.mul(FEE_FRACTION);
		output[0].getField("fee").setValue(amount);
		
		return 0;
	}
}
 