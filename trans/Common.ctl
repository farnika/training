// Test if given garph parameter is empty or undefined
function boolean isEmptyParam(string name) {
	return getParamValue(name) == null || getParamValue(name) == "";
}

// Clamp numeric value into given range. null range boundary (left or right) means infinity (i.e. no clamping from that side)
function number clamp(number value, number min, number max) {
	if (min != null) {
		if (value < min) {
			return min;
		}
	}
	if (max != null) {
		if (value > max) {
			return max;
		}
	}
	
	return value;
}

function integer clampInt(integer value, integer min, integer max) {
	if (min != null) {
		if (value < min) {
			return min;
		}
	}
	if (max != null) {
		if (value > max) {
			return max;
		}
	}
	
	return value;
}

function number getNumericParam(string paramName, number defaultValue) {

	if (!isEmptyParam(paramName)) {
		number result = str2double(getParamValue(paramName));
		printLog(info, paramName + "=" + result);
		return result;
	}
	
	return defaultValue;
}

function integer getIntParam(string paramName, integer defaultValue) {

	if (!isEmptyParam(paramName)) {
		integer result = str2integer(getParamValue(paramName));
		printLog(info, paramName + "=" + result);
		return result;
	}
	
	return defaultValue;
}
