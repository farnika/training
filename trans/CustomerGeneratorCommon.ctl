function string fixName(string input) {
	return upperCase(left(input, 1)) + right(lowerCase(input), length(input) - 1);
}